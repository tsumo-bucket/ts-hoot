@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item far">
            <a href="{{ route('adminDashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item far">
            Page
        </li>
        <li class="breadcrumb-item far active" aria-current="page">
            @if (@$page->allow_add_contents == 1)
                {{ $title }}
            @else
                {{ $content->name }}
            @endif
        </li>
    </ol>
</nav>
@stop 
@section('header')
<header class="flex-center">
    <h1>
      {{ $title }}
    </h1>
    <div class="header-actions">
        <a href="{{route('adminPages', @$page->slug)}}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>        
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated submit-form-btn"
            data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</header>
@stop 

@section('footer')
<footer>
    <div class="text-right">
        <a href="{{route('adminPages', @$page->slug)}}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>        
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated submit-form-btn"
            data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</footer>
@stop

@section('content')
  {!! Form::model($data, ['route'=>['adminClientPageContentUpdate', @$page->slug, $content->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit form-clear']) !!}
    @include('admin.page_contents.form')
  {!! Form::close() !!}
@stop



<?php
// @section('header')
// <header class="flex-center">
//     <h1>{{ $title }}</h1>
//     @if(!@$data->seo)
//         <div class="header-actions">
//             <a href="{{ route('adminPageContents') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
//             <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
//         </div>
//     @endif
// </header>
// @stop

// @if(!@$data->seo)
//     @section('footer')
//     <footer>
//         <div class="text-right">
//             <a href="{{ route('adminPageContents') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
//             <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
//         </div>
//     </footer>
//     @stop
// @endif

// @section('content')
//     <div class="row">
//         @if(@$data->seo)
//         <div class="col-sm-8">
//             <div class="caboodle-card">
//                 <div class="caboodle-card-body">
//                     {!! Form::model($data, ['route'=>['adminPageContentsUpdate', $data->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit']) !!}
//                     @include('admin.page_contents.form')
//                     {!! Form::close() !!}
//                     <footer>
//                         <div class="text-right">
//                             <a href="{{ route('adminPageContents') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
//                             <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
//                         </div>
//                     </footer>
//                 </div>
//             </div>
//         </div>
//         <div class="col-sm-4">
// 			<div class="caboodle-card">
//                 <div class="caboodle-card-header">
//                 <h4 class="no-margin"><i class="far fa-globe"></i> SEO</h4>
//                 </div>
// 				<div class="caboodle-card-body">
// 					<div class="seo-url" data-url="{{route('adminPageContentsSeo')}}">
// 						@include('admin.seo.form')
// 					</div>
// 				</div>
// 			</div>
// 		</div>
//         @else
//         <div class="col-sm-12">
//             <div class="caboodle-card">
//                 <div class="caboodle-card-header">
//                 <h4 class="no-margin"><i class="far fa-file-alt"></i> FORM</h4>
//                 </div>
//                 <div class="caboodle-card-body">
//                 {!! Form::model($data, ['route'=>['adminPageContentsUpdate', $data->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit']) !!}
//                 @include('admin.page_contents.form')
//                 {!! Form::close() !!}
//                 </div>
//             </div>
//         </div>
//         @endif
//     </div>
// @stop

?>