@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far active"><span>Page Contents</span></li>
  </ol>
</nav>
@stop

@section('header')

<header class="flex-center">
	<h1>{{ $title }}</h1>
	@if($page->allow_add_contents == 1)
	<div class="header-actions">
		<a
			class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
			data-mdc-auto-init="MDCRipple"
			href="{{route('adminClientPageContentCreate', $page->slug)}}"
			permission-action="create"
		>
			Create
		</a>
	</div>
	@endif
</header>

@stop

@section('content')
<div class="row">
	@if ($page->allow_add_contents == 1 || count($data) > 0)
	<div class="col-sm-7">
		<div class="caboodle-card">
			<!-- <div class="caboodle-card-header">
			</div> -->
			<div class="flex align-center pad-top">
				<div class="flex-1 text-right no-margin margin-right">
					Publish
				</div>
				<div>
					<div class="mdc-switch no-margin">
						{!! Form::checkbox('published', '1', @$page->published == 'published', ['class'=>'mdc-switch__native-control', 'data-id' => $page->id]) !!}
						<div class="mdc-switch__background">
							<div class="mdc-switch__knob"></div>
						</div>
					</div>
				</div>
			</div>
			{!! Form::open(['route'=>['adminClientPageContentDestroy', $page->slug], 'method' => 'delete', 'class'=>'form form-parsley form-delete']) !!}
				<div class="caboodle-card-body">
					@if (count($data) > 0) 
					{!! Form::open(['route'=>'adminPages', 'method' => 'get']) !!}
					<table class="caboodle-table">
						<thead>
							<tr>
								<th width="30px"></th>
								@if($page->allow_add_contents == 1)
								<th width="50px">
	                                <div class="mdc-form-field" data-toggle="tooltip" title="Select all products">
	                                    <div class="mdc-checkbox caboodle-table-select-all">
	                                        <input type="checkbox" class="mdc-checkbox__native-control" name="select_all"/>
	                                        <div class="mdc-checkbox__background">
	                                            <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
	                                            <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
	                                            </svg>
	                                            <div class="mdc-checkbox__mixedmark"></div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </th>
	                            <th colspan="1" class="caboodle-table-col-action hide">
	                                <div class="dropdown actions-dropdown">
	                                    <button class="caboodle-btn caboodle-btn-medium caboodle-btn-cancel mdc-button mdc-ripple-upgraded btn-custom-width" type="button" id="tableActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-mdc-auto-init="MDCRipple">
	                                        Actions <i class="fas fa-caret-down"></i>
	                                    </button>
	                                    <div class="dropdown-menu" aria-labelledby="tableActions">
	                                        <a class="dropdown-item caboodle-table-action" 
	                                            href="#"
	                                            data-toggle-alert="warning"
	                                            data-alert-form-to-submit=".form-delete"
	                                            permission-action="delete"
	                                            >Delete Content</a>
	                                    </div>
	                                </div>
	                            </th>
								@endif
								<th class="caboodle-table-col-header">Name</th>
								<th width="48px"></th>
							</tr>
						</thead>
						<tbody id="sortable" sortable-data-url="{{route('adminPageContentsOrder')}}">
							@foreach ($data as $d)
								<tr sortable-id="page_contents-{{$d->id}}">
									<td>
										<i style="color:#00a09a;" class="mr-3 fa fa-th-large sortable-icon" aria-hidden="true"></i>
									</td>
									@if($page->allow_add_contents == 1)
										<td>
											<div class="mdc-form-field">
												<div class="mdc-checkbox">
													<input type="checkbox" class="mdc-checkbox__native-control" name="ids[]" value="{{ $d->id }}"/>
													<div class="mdc-checkbox__background">
														<svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
															<path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
														</svg>
														<div class="mdc-checkbox__mixedmark"></div>
													</div>
												</div>
											</div>
										</td>
									@endif
									<td>
										@if($page->allow_add_contents == 1)
											@if (@$d->controls()->first()->type == "asset")
												<img src="{{asset(@$d->controls()->first()->asset->path)}}" style="width: 100px; height: 80px; object-fit: contain;"/>
											@else
												{{@$d->controls()->first()->value }}
											@endif
										@else
											{{$d->name}}
										@endif
									</td>
									<td class="text-center">
										<a
											href="{{route('adminClientPageContentEdit', [$page->slug,$d->id])}}"
											class="mdc-icon-toggle animated-icon"
											data-toggle="tooltip"
											@if($d->editable == 1)
											title="Edit"
											@else
											title="View"
											@endif
											role="button"
											aria-pressed="false"
											permission-action="edit"
										>
											@if($d->editable == 1)
											<i class="far fa-edit" aria-hidden="true"></i>
											@else
											<i class="far fa-external-link" aria-hidden="true"></i>
											@endif
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					
					{!! Form::close() !!} 
					@else
						@if($page->allow_add_contents == 0)
						<div class="empty text-center">
							No results found
						</div>
						@else
						<a href="{{route('adminClientPageContentCreate', $page->slug)}}">
							<div class="empty-message {{$data->count() > 0 ? 'hide' : ''}}" role="button">
								Add a page content here...
							</div>
						</a>
						@endif
					@endif
				</div>
			{!! Form::close() !!}
		</div>
	</div>
	@endif
	<div class="col-sm-5">
	
		@include('admin.seo.form')
	</div>
</div>

@stop

@section ('added-scripts')
@include('admin.pages.partials.added-script-ordering')
	<script>

		$('body').on('input','input[name=published]',function(){
            var input = $(this);
            var id = input.data('id');
            var status = (input.prop('checked')) ? 'published' : 'draft' ;

            $.ajax({
                type: "POST",
                headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"}, 
                url: "{!! route('adminClientPageToggle') !!}",
                data: {id: id, status: status},
                dataType: "JSON",
                success: function(res){
                    if(res.success){
                        console.log(res.message);
                    }else{
                        console.log(res.message);
                    };
                }, error: function(xhr){
                    console.log(xhr);
                }
            });
		});
	</script>
@stop


<?php
// @section('header')
// <header class="flex-center">
//     <h1>{{ $title }}</h1>
//     <div class="header-actions">
//         <a
//             class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
//             data-mdc-auto-init="MDCRipple"
//             href="{{ route('adminPageContentsCreate') }}"
//         >
//             Create
//         </a>
//     </div>
// </header>
// @endsection

// @section('content')
// <div class="row">
//   <div class="col-sm-12">
//     <div class="caboodle-card">
//       <div class="caboodle-card-header">
//         <div class="filters no-padding">
//           {!! Form::open(['route'=>'adminPageContents', 'method' => 'get', 'class'=>'no-margin']) !!}
//             <div class="caboodle-form-group caboodle-flex caboodle-flex-row caboodle-flex-left caboodle-form-control-connected">
//                 <label class="no-margin single-search no-padding">
//                     {!! Form::text('page_id', null, ['class'=>'form-control input-sm no-margin', 'placeholder'=>'Keyword']) !!}
//                     <button>
//                         <i class="fa fa-search"></i>
//                     </button>
//                 </label>
//                 <!-- {!! Form::hidden('sort', @$sort) !!} {!! Form::hidden('sortBy', @$sortBy) !!} -->
//             </div>
//           {!! Form::close() !!}
//         </div>
//       </div>
//       <div class="caboodle-card-body">
//         @if(count($data) > 0)
//           {!! Form::open(['route'=>'adminPageContentsDestroy', 'method' => 'delete', 'class'=>'form form-parsley form-delete']) !!}
//             <table class="caboodle-table">
//               <thead>
//                 <tr>
//                   <th width="50px">
//                     <div class="mdc-form-field" data-toggle="tooltip" title="Select All">
//                         <div class="mdc-checkbox caboodle-table-select-all">
//                             <input type="checkbox" class="mdc-checkbox__native-control" name="select_all" />
//                             <div class="mdc-checkbox__background">
//                                 <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
//                                     <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
//                                 </svg>
//                                 <div class="mdc-checkbox__mixedmark"></div>
//                             </div>
//                         </div>
//                     </div>
//                   </th>
//                   <th class="caboodle-table-col-action">
//                     <a class="caboodle-btn caboodle-btn-icon caboodle-btn-danger mdc-button mdc-ripple-upgraded mdc-button--unelevated x-small uppercase" 
//                             data-mdc-auto-init="MDCRipple"
//                             href="{{ route('adminPageContentsDestroy') }}"
//                             method="DELETE"
//                             data-toggle-alert="warning"
//                             data-alert-form-to-submit=".form-delete"
//                             permission-action="delete"
//                             data-notif-message="Deleting...">
//                         <i class="fas fa-trash"></i>
//                     </a>
//                   </th>
//                   <th class="caboodle-table-col-header hide" >Page id</th>
//                                             <th class="caboodle-table-col-header hide" >Reference id</th>
//                                             <th class="caboodle-table-col-header hide" >Reference type</th>
                    
//                   <th colspan="100%" ></th>
//                 </tr>
//               </thead>
//               <tbody>
//                 @foreach($data as $d)
//                   <tr>
//                     <td>
//                       <div class="mdc-form-field">
//                         <div class="mdc-checkbox">
//                             <input type="checkbox" class="mdc-checkbox__native-control" name="ids[]" value="{{ $d->id }}" />
//                             <div class="mdc-checkbox__background">
//                                 <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
//                                     <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
//                                 </svg>
//                                 <div class="mdc-checkbox__mixedmark"></div>
//                             </div>
//                         </div>
//                       </div>
//                     </td>
//                     <td >
//                     {{$d->page_id}}            </td>
//                                     <td class="uppercase sub-text-1" >{{$d->reference_id}}</td>
//                                     <td class="uppercase sub-text-1" >{{$d->reference_type}}</td>
                     
//                     <td width="110px" class="text-center">
//                       @if (Auth::user()->type == 'super')
//                         <a href="{{route('adminPageContentsEdit', [$d->id])}}" 
//                             class="mdc-icon-toggle animated-icon" 
//                             data-toggle="tooltip"
//                             title="Manage"
//                             role="button"
//                             aria-pressed="false"
//                             permission-action="edit">
//                             <i class="far fa-edit" aria-hidden="true"></i>
//                         </a>
//                       @endif
//                     </td>
//                   </tr>
//                 @endforeach
//               </tbody>
//             </table>
//           {!! Form::close() !!}
//         @else
//           <div class="empty text-center">
//               No results found
//           </div>
//         @endif
//         @if ($pagination)
//           <div class="caboodle-pagination">
//               {{$data->links('layouts.pagination')}}
//           </div>
//         @endif
//       </div>
//     </div>
//   </div>
// </div>
// @stop
?>